### hackall.script

- Will hack all the servers in the list
- Uploads and runs `hack.js` (threads calculated by server ram)

### purchaseServers.script

- Will purchase 8gb ram servers until max
- Uploads and runs `hack.js` (6 threads)

### hack.js

- Hack script template
- Until hacking level xx set target to `n00dles`
- From level xx upwards set target to `joeguns`
- from ... set target to ``

- Will `hack`, `weaken` and `grow` the target
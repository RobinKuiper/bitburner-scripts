const target = "joesguns";

export async function main(ns) {
  const {
    getServerMaxMoney,
    getServerMinSecurityLevel,
    fileExists,
    brutessh,
    nuke,
    getServerMoneyAvailable,
    getServerSecurityLevel,
    weaken,
    grow,
    hack
  } = ns;

  // Defines how much money a server should have before we hack it
  // In this case, it is set to 75% of the server's max money
  const moneyThreshold = getServerMaxMoney(target) * 0.75;

  // Defines the maximum security level the target server can
  // have. If the target's security level is higher than this,
  // we'll weaken it before doing anything else
  const securityTreshold = getServerMinSecurityLevel(target) + 5;

  // If we have the BruteSSH.exe program, use it to open the SSH Port
  // on the target server
  if (fileExists("BruteSSH.exe", "home")) {
    await brutessh(target);
  }

  // Get root access to target server
  await nuke(target);

  // Infinite loop that continously hacks/grows/weakens the target server
  while (true) {
    if (getServerSecurityLevel(target) > securityTreshold) {
      // If the server's security level is above our threshold, weaken it
      await weaken(target);
    } else if (getServerMoneyAvailable(target) < moneyThreshold) {
      // If the server's money is less than our threshold, grow it
      await grow(target);
    } else {
      // hack it.
      await hack(target);
    }
  }
}